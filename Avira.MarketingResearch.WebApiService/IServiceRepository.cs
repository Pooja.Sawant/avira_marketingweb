﻿using System.Net.Http;

namespace Avira.MarketingResearch.WebApiService
{
    public interface IServiceRepository
    {
        HttpResponseMessage GetResponse(string segment);
        HttpResponseMessage PutResponse(string segment, object model);
        HttpResponseMessage PostResponse(string segment, object model);
        HttpResponseMessage DeleteResponse(string segment);
    }
}
