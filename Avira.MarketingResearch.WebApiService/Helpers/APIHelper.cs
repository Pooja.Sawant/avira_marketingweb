﻿using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Helpers
{
    public class APIHelper : IAPIHelper
    {
        IConfiguration _configuration;
        public APIHelper(IConfiguration iconfiguration)
        {
            _configuration = iconfiguration;
        }

        public string GetBaseURL()
        {
            return _configuration.GetSection("APIUrls")["BaseUrl"];
        }

        public string GetSegmentURL(string segmentName)
        {
            segmentName = segmentName + "URL";
            string relativeURL = _configuration.GetSection("APIUrls")[segmentName];
            return relativeURL;
        }
    }
}
