﻿namespace Avira.MarketingResearch.Web.Helpers
{
    public interface IAPIHelper
    {
        string GetBaseURL();
        string GetSegmentURL(string segmentName); 
    }
}
