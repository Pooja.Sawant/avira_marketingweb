﻿using Avira.MarketingResearch.Web.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;

namespace Avira.MarketingResearch.WebApiService
{
    public class ServiceRepository : IServiceRepository
    {
        protected IAPIHelper _apiHelper;
        public HttpClient Client { get; set; }

        public ServiceRepository(IAPIHelper apiHelper)
        {
            _apiHelper = apiHelper;
            Client = new HttpClient();
            Client.BaseAddress = new Uri(_apiHelper.GetBaseURL());
        }
        public HttpResponseMessage GetResponse(string segment)
        {
            string segmentURL = _apiHelper.GetSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage PutResponse(string segment, object model)
        {
            return new HttpResponseMessage { };
            //return Client.PutAsync(url).Result;
        }
        public HttpResponseMessage PostResponse(string segment, object model)
        {
            return new HttpResponseMessage { };
            //return Client.PostAsync(url, model).Result;
        }
        public HttpResponseMessage DeleteResponse(string segment)
        {
            return Client.DeleteAsync(segment).Result;
        }
    }
}
