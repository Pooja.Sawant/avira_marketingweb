﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Avira.MarketingResearch.Model
{
    public class RawMaterialViewModel
    {
        public string RawMaterialName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public string RowType
        {
            get
            {
                return "RawMaterial";
            }
        }

    }

    public class RawMaterialInsertDbViewModel
    {
        [Required]
        public string RawMaterialName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
    }

    public class RawMaterialUpdateDbViewModel
    {
        [Required]
        public string RawMaterialName { get; set; }
        public string Description { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
    }
}
