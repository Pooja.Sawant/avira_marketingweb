﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.WebApiService;
using System.Net.Http;
using Avira.MarketingResearch.Web.Models;

namespace Avira.MarketingResearch.Web.Controllers
{
    public class HomeController : Controller
    {
        public readonly IServiceRepository _serviceRepository;
        public HomeController(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public IActionResult Index()
        {
            HttpResponseMessage response = _serviceRepository.GetResponse("RawMaterial");
            response.EnsureSuccessStatusCode();
            List<RawMaterialViewModel>products = response.Content.ReadAsAsync<List<RawMaterialViewModel>>().Result;
            ViewBag.Title = "All Products";

            //response.EnsureSuccessStatusCode();
            //Models.Product products = response.Content.ReadAsAsync<Models.Product>().Result;
            //ViewBag.Title = "All Products";
            //return View(products);
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
